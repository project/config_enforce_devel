<?php

declare(strict_types=1);

namespace Drupal\config_enforce_devel;

/**
 * Provides integration with config_devel.
 */
class ConfigDevelHelper {

  /**
   * Provide Config Devel module with configs to read from disk.
   *
   * @see \config_enforce_devel_config_devel_auto_import_files()
   */
  public static function autoImport() {
    $auto_import = [];
    foreach ((new EnforcedConfigCollection())->getEnforcedConfigs() as $config_name => $settings) {
      $auto_import[] = [
        'filename' => $settings['config_file_path'],
        'hash' => isset($settings['hash']) ? $settings['hash'] : '',
      ];
    }
    return $auto_import;
  }

  /**
   * Provide Config Devel module with config files to write to disk.
   *
   * @see \config_enforce_devel_config_devel_auto_export_files()
   */
  public static function autoExport() {
    $auto_export = [];
    foreach ((new EnforcedConfigCollection())->getEnforcedConfigs() as $config_name => $settings) {
      $auto_export[] = $settings['config_file_path'];
    }
    return $auto_export;
  }

  /**
   * Save hashes of auto-imported config files.
   *
   * @see \config_enforce_devel_config_devel_post_auto_import()
   */
  public static function postAutoImport(array $auto_import) {
    $registries = (new TargetModuleCollection())->getRegistryConfigNames();
    $enforced_configs = (new EnforcedConfigCollection())->getEnforcedConfigs();
    $hashes = [];
    foreach ($auto_import as $config_name => $results) {
      // Skip registry configs, since their hashes are hard-coded.
      // @see EnforcedConfigRegistry::updateEnforcedConfigs().
      if (in_array($config_name, $registries)) continue;

      // Skip configs that we don't enforce.
      if (!\array_key_exists($config_name, $enforced_configs)) {
        continue;
      }

      // Check for missing hash elements, and fill them in to ensure they get regenerated.
      if (!isset($results['hash'])) {
        $results['hash'] = 'missing_from_config_devel_result';
      }
      if (!isset($enforced_configs[$config_name]['hash'])) {
        $enforced_configs[$config_name]['hash'] = 'missing_from_config_enforce_registry';
      }

      // Skip configs whose hashes haven't changed or aren't present.
      if ($results['hash'] == $enforced_configs[$config_name]['hash']) continue;

      // Generate a sparse array of hashes to pass to updateEnforcedConfigs to fill in.
      $hashes[$config_name] = ['hash' => $results['hash']];
    }
    (new EnforcedConfigCollection())->updateEnforcedConfigs($hashes);
  }

}
