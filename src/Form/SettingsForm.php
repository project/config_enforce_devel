<?php

declare(strict_types=1);

namespace Drupal\config_enforce_devel\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\config_enforce_devel\EnforcedConfig;
use Drupal\config_enforce_devel\Form\DevelFormHelperTrait;
use Drupal\config_enforce_devel\TargetModuleCollection;

/**
 * Defines a settings form for the Config Enforce Devel module.
 */
class SettingsForm extends ConfigFormBase {

  use DevelFormHelperTrait;

  const FORM_ID = 'config_enforce_devel_settings';

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'config_enforce_devel.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $this->setSharedFormProperties($form, $form_state);

    $this->addAvailableModulesField();
    $this->addDefaultsFields();
    $this->addIgnoredConfigsField();

    return parent::buildForm($this->form(), $this->formState());
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->setSharedFormProperties($form, $form_state);

    $this->config('config_enforce_devel.settings')
      ->set('defaults', $this->formState()->getValue('defaults'))
      ->set('ignored_configs', array_values($this->formState()->getValue('ignored_configs')))
      ->save();

    $available_modules = $this->formState()->getValue('available_modules');
    $target_modules = $this->getEnforcedConfigCollection()->getTargetModuleCollection();
    $target_modules
      ->ensureTargetModulesAreRegistered($available_modules);

    $default_target_module = $target_modules->getDefaultTargetModule();
    if (!in_array($default_target_module, $available_modules)) {
      $warning = $this->t('Default target module (@default) cannot be de-selected.', [
        '@default' => $default_target_module,
      ]);
      $this->messenger()->addWarning($warning);
    }

    return parent::submitForm($this->form(), $this->formState());
  }

  /**
   * Add field to select available modules.
   */
  protected function addAvailableModulesField() {
    $this->form()['target_modules'] = [
      '#type' => 'details',
      '#open' => FALSE,
      '#title' => $this->t('Target modules'),
    ];

    $this->form()['target_modules']['available_modules'] = [
      '#type' => 'multiselect',
      '#title' => $this->t('Select available target modules'),
      '#description' => $this->t('These modules will be available as target modules on config forms, to specify where configuration files should be written.<br />Note that you cannot de-select the default target module (see the defaults below).'),
      '#options' => $this
        ->getEnforcedConfigCollection()
        ->getTargetModuleCollection()
        ->getAllModules(),
      '#multiple' => TRUE,
      '#default_value' => $this
        ->getEnforcedConfigCollection()
        ->getTargetModuleCollection()
        ->getTargetModuleNames(),
    ];
  }

  /**
   * Add fields to define defaults for enforcement settings fields.
   */
  protected function addDefaultsFields() {
    $this->form()['defaults'] = [
      '#type' => 'details',
      '#open' => FALSE,
      '#tree' => TRUE,
      '#title' => $this->t('Enforcement field defaults'),
    ];
    $this->form()['defaults']['target_module'] = $this->getTargetModuleField();
    $this->form()['defaults']['config_directory'] = $this->getConfigDirectoryField();
    $this->form()['defaults']['enforcement_level'] = $this->getEnforcementLevelField();
    $this->form()['defaults']['enforce_dependencies'] = $this->getEnforceDependenciesField();
  }

  /**
   * Add field to select available modules.
   */
  protected function addIgnoredConfigsField() {
    $this->form()['unenforced_configs'] = [
      '#type' => 'details',
      '#open' => FALSE,
      '#title' => $this->t('Unenforced configs'),
    ];

    $this->form()['unenforced_configs']['ignored_configs'] = [
      '#type' => 'multiselect',
      '#title' => $this->t('Select config to ignore'),
      '#description' => $this->t('These config will not appear in lists of unenforced configs. '),
      '#options' => $this->getAllUnenforcedConfigs(),
      '#multiple' => TRUE,
      '#default_value' => $this->getIgnoredConfigs(),
    ];
  }

}
