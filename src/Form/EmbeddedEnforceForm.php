<?php

declare(strict_types=1);

namespace Drupal\config_enforce_devel\Form;

use Drupal\config_enforce\Form\AbstractEnforceForm;
use Drupal\config_enforce_devel\EnforcedConfig;
use Drupal\config_enforce_devel\Form\DevelFormHelperTrait;
use Drupal\config_enforce_devel\Form\ThemeSettingsFormTrait;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Defines a settings form for the Config Enforce Devel module.
 */
class EmbeddedEnforceForm extends AbstractEnforceForm {

  use DevelFormHelperTrait;

  use ThemeSettingsFormTrait;

  const FORM_ID = 'config_enforce_devel_embedded';

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $this->setSharedFormProperties($form, $form_state);

    if ($this->hasKnownIssues()) {
      $this->addKnownIssuesWarning();
    }

    $this->addConfigFormInfo();

    $form['config_enforce']['actions'] = [
      '#type' => 'actions',
      'submit'  => [
        '#type'   => 'submit',
        '#button_type'  => 'primary',
        '#value' => $this->t(
          'Save enforced configuration settings'
        )
      ],
    ];

    $form = $this->buildFormThemeSettings($form);

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  protected function getConfigFormUri() {
    // Since we're on the host form, we have a chance to record the URI.
    return \Drupal::request()->getPathInfo();
  }

  /**
   * Return a warning that the host form may be buggy.
   */
  protected function addKnownIssuesWarning() {
    $issues = [];
    foreach ($this->getKnownIssues() as $id => $title) {
      $url = Url::fromUri('https://www.drupal.org/node/' . $id);
      $link = Link::fromTextAndUrl('Issue #' . $id . ': ' . $title, $url);
      $issues[] = $link;
    }

    $this->addWarning(
      'This configuration form (%form_id) may not yet be fully supported.<br /> For more information, review the following @issue: @issue_list',
      [
        '%form_id' => $this->getContext('form_id'),
        '@issue' => count($issues) > 1 ? $this->t('issues') : $this->t('issue'),
        '@issue_list' => $this->renderHtmlList($issues),
      ]
    );
  }

  /**
   * Determine whether the host form has any known issues.
   */
  protected function hasKnownIssues() {
    return array_key_exists($this->getContext('form_id'), $this->getAllKnownIssues());
  }

  /**
   * Return a list of all known issues for the host form.
   */
  protected function getKnownIssues() {
    return $this->getAllKnownIssues()[$this->getContext('form_id')];
  }

  /**
   * Return a list of all known issues for all forms that are known to be buggy.
   */
  protected function getAllKnownIssues() {
    // @TODO Add a test fixture module form here.
    return [];
  }

}
