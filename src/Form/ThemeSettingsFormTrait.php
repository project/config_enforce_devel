<?php

declare(strict_types=1);

namespace Drupal\config_enforce_devel\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Trait for working around theme settings form fatal error.
 *
 * @see https://www.drupal.org/project/config_enforce_devel/issues/3355729
 *   Issue detailing the error and solution.
 *
 * @see https://www.drupal.org/project/config_enforce_devel/issues/3188474
 *   Issue detailing plan to pull the embedded enforce form out of the parent
 *   form and into a dialog or off-canvas panel. When this lands, these traits
 *   and use of them will be removed.
 *
 * @see \Drupal\config_enforce_devel\FormHandler\ThemeSettingsFormHandlerTrait
 *   Counterpart trait.
 *
 * @see \Drupal\system\Form\ThemeSettingsForm::submitForm()
 *   This passes all theme settings to \theme_settings_convert_to_config().
 *
 * @see \theme_settings_convert_to_config()
 *   Unconditionally attempts to save all configuration, thus resulting in our
 *   embedded enforce form values (which will contain a dot) causing a fatal
 *   error.
 *
 * @todo Remove this and its counterpart trait when we pull the embedded enforce
 *   form into its own separate form.
 */
trait ThemeSettingsFormTrait {

  /**
   * Alter the embedded enforce form for theme settings form workaround.
   *
   * Note that this performs the check for whether the host form is the theme
   * settings form so no check is needed before calling this.
   *
   * @param array $form
   *   The un-altered embedded enforce form.
   *
   * @return array
   *   The $form parameter with modifications if the host form is the theme
   *   settings form.
   */
  protected function buildFormThemeSettings(array $form): array {

    if ($this->getContext('form_id') !== 'system_theme_settings') {
      return $form;
    }

    $form['config_enforce']['actions']['submit']['#config_enforce_devel_submit'] = true;

    return $form;

  }

  /**
   * Determine if our submit handler should be skipped on a theme settings form.
   *
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The form state at the time of our submit handler being invoked.
   *
   * @return bool
   *   True if this is the theme settings form and it was not submitted by our
   *   submit button; false if this is the theme settings form but our submit
   *   button was used; false if this is any other form.
   */
  protected function shouldSkipThemeSettingsSubmit(
    FormStateInterface $formState
  ): bool {

    if (
      $this->getContext('form_id') === 'system_theme_settings' &&
      empty($formState->getTriggeringElement()['#config_enforce_devel_submit'])
    ) {
      return true;
    }

    return false;

  }

  /**
   * Escape any config keys in our container on theme settings form submit.
   *
   * This prevents Drupal core throwing an error if it tries to save our keys
   * as config as they very likely contain a dot which is an invalid character
   * in config keys.
   *
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The form state at the time of our submit handler being invoked.
   */
  protected function submitFormThemeSettingsEscapeKeys(
    FormStateInterface $formState
  ): void {

    if ($this->getContext('form_id') !== 'system_theme_settings') {
      return;
    }

    foreach ($this->getContext('configs') as $configKey) {

      $formState->setValue(
        ['config_enforce', $configKey],
        $formState->getValue(
          ['config_enforce', \str_replace('.', ':', $configKey)]
        )
      );

      $formState->unsetValue(
        ['config_enforce', \str_replace('.', ':', $configKey)]
      );

    }

  }

  /**
   * Remove our values on the theme settings form after saving enforce settings.
   *
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The form state at the time of our submit handler being invoked.
   */
  protected function removeThemeSettingsFormValue(
    FormStateInterface $formState
  ): void {

    // Remove our values from the form state at this point to prevent the theme
    // settings form submit handler including our values in the theme's
    // configuration.
    if ($this->getContext('form_id') === 'system_theme_settings') {
      $formState->unsetValue('config_enforce');
    }

  }

}
