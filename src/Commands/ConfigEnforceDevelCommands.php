<?php

declare(strict_types=1);

namespace Drupal\config_enforce_devel\Commands;

use Drupal\config_enforce_devel\EnforcedConfigFile;
use Drupal\config_enforce_devel\TargetModuleCollection;
use Drush\Attributes as CLI;
use Drush\Boot\DrupalBootLevels;
use Drush\Commands\DrushCommands;

/**
 * The Drush commandfile for Config Enforce.
 *
 * See these files for an example of injecting Drupal services:
 *   - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 *   - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */
class ConfigEnforceDevelCommands extends DrushCommands {

  /**
   * Update exported configs from active storage.
   */
  #[CLI\Command(name: 'config-enforce:update', aliases: ['ceu'])]
  #[CLI\Bootstrap(level: DrupalBootLevels::MAX)]
  #[CLI\Usage(name: 'drush config-enforce:update', description: 'Update configurations on disk from active storage.')]
  public function update() {
    $modules = (new TargetModuleCollection())->getTargetModules();
    foreach ($modules as $key => $module) {
      $registry = $module->getRegistry();
      $configs_to_update = [];
      foreach ($registry->getEnforcedConfigs() as $config_name => $settings) {
        unset($settings['hash']);
        $this->logger()->info(dt("Writing config from active storage for: %config", ['%config' => $config_name]));
        (new EnforcedConfigFile($config_name, $settings))->writeConfigFile();
        $configs_to_update[$config_name] = $settings;
      }
      $this->logger()->info(dt("Updating hashes of enforced configs for '%module' module.", ['%module' => $key]));
      $registry->updateEnforcedConfigs($configs_to_update);
      $this->logger()->notice(dt("Updated enforced configs and hashes for '%module' module.", ['%module' => $key]));
    }
    $this->logger()->success(dt('Updated enforced configs from active storage.'));
  }

}
