<?php

declare(strict_types=1);

namespace Drupal\config_enforce_devel;

use Drupal\config_enforce\ConfigEnforceHelperTrait;
use Drupal\Core\Config\InstallStorage;
use Drupal\Core\Extension\MissingDependencyException;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\config_enforce\ConfigEnforcer;

/**
 * Create and manage a target module.
 */
class TargetModuleBuilder {

  // Channel with which to log from this class.
  use ConfigEnforceHelperTrait;

  const LOGCHANNEL = 'config_enforce_devel\TargetModuleBuilder';

  // The target module's human-readable name.
  protected $name;

  // The target module's machine-readable name.
  protected $machineName;

  // The target module's description.
  protected $description;

  // The path to which target modules should be written.
  protected $path;

  // An instance of the Drupal file system service.
  protected $fileSystem;

  /**
   * The Drupal renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected RendererInterface $renderer;

  /**
   * A basic constructor method.
   */
  public function __construct() {
    $this->fileSystem = \Drupal::service('file_system');
    $this->renderer   = \Drupal::service('renderer');
  }

  /**
   * Set the target module's human-readable name.
   */
  public function setName(TranslatableMarkup $name) {
    $this->name = $name;
    return $this;
  }

  /**
   * Return the target module's human-readable name.
   */
  public function getName() {
    return $this->name;
  }

  /**
   * Set the target module's machine-readable name.
   */
  public function setMachineName(string $machine_name) {
    $this->machineName = $machine_name;
    return $this;
  }

  /**
   * Return the target module's machine-readable name.
   */
  public function getMachineName() {
    return $this->machineName;
  }

  /**
   * Set the target module's description.
   */
  public function setDescription(TranslatableMarkup $description) {
    $this->description = $description;
    return $this;
  }

  /**
   * Return the target module's description.
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * Set the path to which target modules should be written.
   */
  public function setPath(string $path) {
    $this->path = $path;
    return $this;
  }

  /**
   * Return the path to which target modules should be written.
   */
  public function getPath() {
    return $this->path;
  }

  /**
   * Determine whether the human-readable name is unique.
   */
  public function nameIsUnique() {
    return !in_array($this->getName(), (new TargetModuleCollection())->getAllModules());
  }

  /**
   * Determine whether the machine name is unique.
   */
  public function machineNameIsUnique() {
    return !array_key_exists($this->getMachineName(), (new TargetModuleCollection())->getAllModules());
  }

  /**
   * Determine whether the machine name respect the maximum allowed length.
   */
  public function machineNameRespectsMaxLength() {
    return strlen($this->getMachineName()) <= DRUPAL_EXTENSION_NAME_MAX_LENGTH;
  }

  /**
   * Determine whether the target module directory already exists.
   */
  public function directoryPathExists() {
    return is_dir($this->getDirectoryPath());
  }

  /**
   * Create a new target module.
   */
  public function createModule() {
    $this->prepareDirectory();
    $this->writeInfoFile();

    $message = $this->t('Created %name (@machine_name) module at %path.', [
      '%name' => $this->getName(),
      '@machine_name' => $this->getMachineName(),
      '%path' => $this->getDirectoryPath(),
    ]);
    $this->log(self::LOGCHANNEL)->notice($message);
    $this->messenger()->addStatus($message);

    # Having created a new module, explicitly add its path to the extension
    # list and reset caches.
    $module_list = \Drupal::service('extension.list.module');
    $module_list->setPathName($this->getMachineName(), $this->getInfoFilePath());
    $module_list->reset();

    return $this;
  }

  /**
   * Enable the new target module.
   */
  public function installModule() {
    try {
      // We should be able to just call core methods to install the module:
      //   \Drupal::service('module_installer')
      //     ->install([$this->getMachineName()], FALSE);
      //
      // However, this results in a \LogicException that we appear not to be able to catch.
      // This appears related to https://www.drupal.org/project/drupal/issues/2531564
      //
      // So, instead, we've copied in a bunch of code from ModuleInstaller from
      // core (v9.2.3).  We simplified somewhat for our use-case, and stuck it
      // in our own class.
      //
      // Eventually, we should be able to go back to using the core class and
      // method. At that point, we can just delete this new class.
      (new TargetModuleInstaller())->install($this->getMachineName());
    } catch (\Exception $e) {
      $message = $this->t('%name (@machine_name) target extension did not install properly. (@message)', [
        '%name' => $this->getName(),
        '@machine_name' => $this->getMachineName(),
        '@message' => $e->getMessage(),
      ]);
      $this->log(self::LOGCHANNEL)->error($message);
      $this->messenger()->addError($message);
      return $this;
    }

    $message = $this->t('Installed %name (@machine_name) module.', [
      '%name' => $this->getName(),
      '@machine_name' => $this->getMachineName(),
    ]);
    $this->log(self::LOGCHANNEL)->notice($message);

    return $this;
  }

  /**
   * Register the new module as a target for writing config.
   */
  public function registerAsTargetModule() {
    $this->initializeEnforcedConfigs();
    drupal_static_reset('Drupal\config_enforce\TargetModuleCollection::getTargetModules');
    return $this;
  }

  /**
   * Create the initial enforced configs for the target module.
   */
  public function initializeEnforcedConfigs() {
    $module = $this->getMachineName();
    $registry = new EnforcedConfigRegistry($module);
    $config_name = $registry->getConfigName();

    // Enforced configs settings must, themselves, be enforced.
    $enforced_configs_to_create = [
      $config_name => [
        // Enforced configs settings are specific to their target module.
        'target_module' => $module,
        // Enforced configs settings must be imported when the terget module is enabled.
        'config_directory' => InstallStorage::CONFIG_INSTALL_DIRECTORY,
        // Enforced configs settings must, themselves, be read-only.
        'enforcement_level' => ConfigEnforcer::CONFIG_ENFORCE_READONLY,
        // While read-only, the enforced configs page is the form where this'll be embedded.
        'config_form_uri' => Url::fromRoute('config_enforce_devel.enforced_configs')->toString(),
      ],
    ];

    $registry->createEnforcedConfigs($enforced_configs_to_create);
    (new TargetModule($module))->initializeNewConfigFiles();

    $message = $this->t('Created config object %config_name for %name (@machine_name) target module enforced configs.', [
      '%config_name' => $config_name,
      '%name' => $this->getName(),
      '@machine_name' => $module,
    ]);
    $this->log(self::LOGCHANNEL)->notice($message);

    return $this;
  }

  /**
   * Ensure that a given directory exists and has the proper permissions.
   *
   * This defaults to the target module's directory, but can be overridden by
   * providing an argument.
   *
   * @param string $directory
   *   (Optional) A directory within the file path to create.
   */
  protected function prepareDirectory(string $directory = '') {
    $options = FileSystemInterface::CREATE_DIRECTORY + FileSystemInterface::MODIFY_PERMISSIONS;
    $directory_path = $this->getDirectoryPath();
    if (!empty($directory)) {
      $directory_path = $directory_path . '/' . $directory;
    }
    $this->fileSystem->prepareDirectory($directory_path, $options);

    return $this;
  }

  /**
   * Return the path of the new target module directory.
   */
  protected function getDirectoryPath() {
    return $this->getPath() . '/' . $this->getMachineName();
  }

  /**
   * Write a module info file.
   */
  protected function writeInfoFile() {
    $file_path = $this->getInfoFilePath();
    $result = $this->fileSystem
      ->saveData($this->getInfoFileContents(), $file_path, FileSystemInterface::EXISTS_REPLACE);

    if ($result === FALSE) {
      $message = $this->t('Failed to write info file %file_path.', [
        '%file_path' => $file_path,
      ]);
      $this->log(self::LOGCHANNEL)->notice($message);
      $this->messenger()->addError($message);
      return $this;
    }

    $timeout = 5; $timer = 0;
    while (!file_exists($file_path)) {
      if ($timer >= $timeout) {
        $message = $this->t('Timed out writing info file %file_path.', [
          '%file_path' => $file_path,
        ]);
        $this->log(self::LOGCHANNEL)->notice($message);
        $this->messenger()->addError($message);
        break;
      }

      sleep(1);
      $timer++;
    }

    return $this;
  }

  /**
   * Return the path of the new target module's info file.
   */
  protected function getInfoFilePath() {
    return $this->getDirectoryPath() . '/' . $this->getMachineName() . '.info.yml';
  }

  /**
   * Return a rendered the info file template.
   */
  protected function getInfoFileContents() {
    $template = \Drupal::service('extension.list.module')->getPath('config_enforce_devel') . '/templates/info.yml.twig';

    /** @var array Render array for the extension .info.yml contents. */
    $renderArray = [
      '#type'     => 'inline_template',
      '#template' => \file_get_contents($template),
      '#context'  => [
        'name'        => $this->getName(),
        'description' => $this->getDescription(),
      ],
    ];

    // Cast to a string since this an object implementing
    // \Drupal\Component\Render\MarkupInterface
    return (string) $this->renderer->renderPlain($renderArray);
  }

}
