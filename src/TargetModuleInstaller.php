<?php

declare(strict_types=1);

namespace Drupal\config_enforce_devel;

use Drupal\Core\Extension\Extension;

/**
 * Install a target module.
 *
 * Much of this was copied from the core ModuleInstaller class.
 * @see: TargetModuleBuilder::installModule().
 */
class TargetModuleInstaller {

  /**
   * Install a new target module.
   */
  public function install($module) {
    $extension_config = \Drupal::configFactory()
      ->getEditable('core.extension');

    // Save this data without checking schema. This is a performance
    // improvement for module installation.
    $extension_config
      ->set("module.{$module}", 0)
      ->set('module', module_config_sort($extension_config
      ->get('module')))
      ->save(TRUE);

    $module_filenames = $this->getModuleFilenames($extension_config);

    // Update the module handler in order to have the correct module list
    // for the kernel update.
    $this->moduleHandler
      ->setModuleList($module_filenames);

    // Clear the static cache of the "extension.list.module" service to pick
    // up the new module, since it merges the installation status of modules
    // into its statically cached list.
    \Drupal::service('extension.list.module')
      ->reset();

    // Update the kernel to include it.
    $this
      ->updateKernel($module_filenames);

    return TRUE;
  }

  /**
   * Return a list of all module filenames, including our new ones, sorted by weight, etc.
   *
   * @see ModuleInstaller::install()
   */
  protected function getModuleFilenames($extension_config) {
    $this->moduleHandler = \Drupal::moduleHandler();
    $current_module_filenames = $this->moduleHandler
      ->getModuleList();
    $current_modules = array_fill_keys(array_keys($current_module_filenames), 0);
    $current_modules = module_config_sort(array_merge($current_modules, $extension_config
      ->get('module')));
    $module_filenames = [];
    foreach ($current_modules as $name => $weight) {
      if (isset($current_module_filenames[$name])) {
        $module_filenames[$name] = $current_module_filenames[$name];
      }
      else {
        $module_path = \Drupal::service('extension.list.module')
          ->getPath($name);
        $pathname = "{$module_path}/{$name}.info.yml";
        $filename = file_exists($module_path . "/{$name}.module") ? "{$name}.module" : NULL;
        $module_filenames[$name] = new Extension(\Drupal::root(), 'module', $pathname, $filename);
      }
    }
    return $module_filenames;
  }

  /**
   * Reboot the kernel to register the new target module.
   */
  protected function updateKernel($module_filenames) {
    $this->kernel = \Drupal::service('kernel');
    $this->kernel
      ->updateModules($module_filenames, $module_filenames);

    // After rebuilding the container we need to update the injected
    // dependencies.
    $container = $this->kernel
      ->getContainer();
    $this->moduleHandler = $container
      ->get('module_handler');
    $this->connection = $container
      ->get('database');
  }

}
