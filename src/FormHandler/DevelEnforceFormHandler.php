<?php

declare(strict_types=1);

namespace Drupal\config_enforce_devel\FormHandler;

use Drupal\config_enforce\FormHandler\AbstractEnforceFormHandler;
use Drupal\config_enforce_devel\FormHandler\ThemeSettingsFormHandlerTrait;
use Drupal\config_enforce_devel\TargetModuleCollection;

/**
 * Attach an embedded enforce form to a host config form.
 */
class DevelEnforceFormHandler extends AbstractEnforceFormHandler {

  use ThemeSettingsFormHandlerTrait;

  /**
   * Method to call from implementations of hook_alter().
   */
  public function alter() {
    // Only operate on config forms.
    if (!$this->isAnEnforceableForm()) return;

    $this->addEnforceForm('Drupal\config_enforce_devel\Form\EmbeddedEnforceForm');
  }

  /**
   * {@inheritdoc}
   *
   * @todo Remove this once the theme settings traits are removed.
   */
  protected function addEnforceForm($class) {

    parent::addEnforceForm($class);

    $this->alterThemeSettingsFormSubmitHandlers();

  }

}
