<?php

declare(strict_types=1);

namespace Drupal\config_enforce_devel\FormHandler;

use Drupal\config_enforce_devel\Form\EmbeddedEnforceForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Theme settings form handler trait.
 *
 * @see \Drupal\config_enforce_devel\Form\ThemeSettingsFormTrait
 *   Counterpart trait; see for details.
 */
trait ThemeSettingsFormHandlerTrait {

  /**
   * Alter the theme settings form submit handlers to prevent fatal errors.
   *
   * This does two things:
   *
   * 1. It re-orders the submit handlers so that our embedded enforce form's
   *    submit handler runs before core's submit handler, and
   *
   * 2. It inserts a new submit handler before both the core and embedded
   *    enforce form handlers to escape periods in config key names to prevent
   *    the core handler causing a fatal error if core ends up trying to save
   *    them along with the theme settings.
   */
  protected function alterThemeSettingsFormSubmitHandlers(): void {

    if ($this->getFormId() !== 'system_theme_settings') {
      return;
    }

    /** @var mixed[] Array of submit handlers for this form. */
    $submitHandlers = &$this->form()['#submit'];

    /** @var false|array False if we can't find our handler; an array with handler callable otherwise. */
    $ourHandler = false;

    foreach ($submitHandlers as $key => $handler) {

      if (
        !\is_array($handler) ||
        count($handler) !== 2 ||
        !\is_object($handler[0]) ||
        // This should really have an interface.
        !($handler[0] instanceof EmbeddedEnforceForm)
      ) {
        continue;
      }

      $ourHandler = $handler;

      // Remove our handler from its current position in the array.
      unset($submitHandlers[$key]);

      break;

    }

    /** @var int|false The core submit handler index, or false if it can't be found. */
    $coreIndex = \array_search('::submitForm', $submitHandlers);

    // Don't do anything if we could find the core handler or our embedded
    // enforce form handler.
    if (!\is_int($coreIndex) || !\is_array($ourHandler)) {
      return;
    }

    // Insert our new handler and then the embedded enforce form's handler right
    // before the core handler, in that order.
    \array_splice(
      $submitHandlers, $coreIndex, 0, [
        [$this, 'submitFormThemeSettings'],
        $ourHandler,
      ]
    );

  }

  /**
   * Theme settings form submit handler.
   *
   * @param array &$form
   *   The form array.
   *
   * @param FormStateInterface $formState
   *   The form state at the time of our submit handler being invoked.
   */
  public function submitFormThemeSettings(
    array &$form, FormStateInterface $formState
  ): void {

    // If this submit was not triggered by our sub-form, completely remove our
    // form value and return here.
    if (empty($formState->getTriggeringElement()[
      '#config_enforce_devel_submit'
    ])) {

      $formState->unsetValue('config_enforce');

      return;

    }

    /** @var array Our form value. */
    $value = &$formState->getValue('config_enforce');

    // Escape the config key so that they don't contain a period to avoid Drupal
    // core throwing a fatal error if it tries to save it.
    $value[
      \str_replace('.', ':', $formState->getValue('config_key'))
    ] = $value[$formState->getValue('config_key')];

    // Remove our original, not escaped key.
    unset($value[$formState->getValue('config_key')]);

  }

}
