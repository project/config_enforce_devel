<?php

declare(strict_types=1);


namespace Drupal\config_enforce_devel;

use Drupal\Core\File\FileSystemInterface;

class EnforcedConfigFile {

  /**
   * @var string The name of the config object.
   */
  protected $configName;

  /**
   * @var array The array of enforcement settings.
   */
  protected $settings;

  /**
   * @var mixed Drupal\Core\FileSystem service
   */
  protected $fileSystem;

  /**
   * @var mixed Drupal\config_devel\EventSubscriber\ConfigDevelAutoExportSubscriber service.
   */
  protected $writeBackSubscriber;

  public function __construct($config_name, $settings) {
    $this->configName = $config_name;
    $this->settings = $settings;
    $this->fileSystem = \Drupal::service('file_system');
    $this->writeBackSubscriber = \Drupal::service('config_devel.writeback_subscriber');
  }

  /**
   * Determine whether a given config file path has changed.
   *
   * @param $new_settings array New enforced config settings.
   *
   * @return bool Whether the config directory or target module path have changed.
   */
  public function configFilePathIsChanged(array $new_settings) {
    $old_path = $this->getDerivedConfigFilePath();
    $new_path = $this->getDerivedConfigFilePath($new_settings);

    return ($old_path != $new_path);
  }

  /**
   * Derive the config file path from the target module and config directory.
   *
   * @param $settings array Enforced config settings.
   *
   * @return string The complete path to the enforced config object on disk.
   */
  public function getDerivedConfigFilePath(array $settings = []) {
    if (empty($settings)) $settings = $this->settings;
    $config_file_path  = \Drupal::service('extension.list.module')->getPath($settings['target_module']);
    $config_file_path .= DIRECTORY_SEPARATOR . $settings['config_directory'] . DIRECTORY_SEPARATOR;
    $config_file_path .= $this->configName . '.yml';
    return $config_file_path;
  }

  /**
   * Write a new config object file to disk in the appropriate target module path.
   */
  public function initialize() {
    if (file_exists($this->settings['config_file_path'])) return;
    $this->writeConfigFile();
  }

  /**
   * Write a config object file to disk.
   */
  public function writeConfigFile() {
    $config = \Drupal::config($this->configName);

    // When updating optional config, it's possible for them not to be in
    // active storage yet. This avoids overwriting such a config file with an
    // empty array.
    if (empty($config->get())) return;

    $this->prepareConfigDirectory()->writeBackSubscriber
      ->writeBackConfig($config, [$this->settings['config_file_path']]);
  }

  /**
   * Ensure that the directory into which a config file should be written, exists.
   */
  protected function prepareConfigDirectory() {
    $directory = $this->fileSystem->dirname($this->getDerivedConfigFilePath());
    $options = FileSystemInterface::CREATE_DIRECTORY + FileSystemInterface::MODIFY_PERMISSIONS;
    $this->fileSystem->prepareDirectory($directory, $options);
    return $this;
  }

  /**
   * Delete the config file path from disk in the target module.
   */
  public function delete() {
    $this->fileSystem->delete($this->getDerivedConfigFilePath());
  }

}
