<?php

declare(strict_types=1);

namespace Drupal\Tests\config_enforce_devel\Functional;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Tests\BrowserTestBase;

/**
 * Form alter implementation order tests.
 *
 * @group config_enforce_devel
 *
 * @todo Is there a better/more general name we could use for this class?
 */
class FormAlterImplementationOrderTest extends BrowserTestBase {

  /**
   * The Drupal module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'config_enforce', 'config_enforce_devel',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {

    parent::setUp();

    $this->moduleHandler = $this->container->get('module_handler');

  }

  /**
   * Test that Config Enforce - Devel's form alter runs after Config Enforce's.
   *
   * @see \config_enforce_devel_module_implements_alter()
   *
   * @see \Drupal\KernelTests\Core\Extension\ModuleImplementsAlterTest::testModuleImplementsAlter()
   *
   * @see \common_test_module_implements_alter()
   */
  public function testFormAlterImplementationOrder(): void {

    $this->assertTrue($this->moduleHandler->hasImplementations(
      'module_implements_alter', 'config_enforce_devel'
    ), 'config_enforce_devel implements module_implements_alter().');

    /** @var string The hook name to pass to the module hander; needs to be passed by reference. */
    $hookName = 'form_alter';

    /** @var array[] Arrays of implementations to be passed by reference to the module handler service for altering. */
    $implementations = [
      'first' => [
        'config_enforce_devel'  => false,
        'nonexistant1'          => false,
        'nonexistant2'          => false,
        'config_enforce'        => false,
        'nonexistant3'          => false,
      ],
      'last' => [
        'nonexistant1'          => false,
        'nonexistant2'          => false,
        'config_enforce'        => false,
        'nonexistant3'          => false,
        'config_enforce_devel'  => false,
      ],
      'mixed' => [
        'nonexistant1'          => false,
        'config_enforce_devel'  => false,
        'nonexistant2'          => false,
        'config_enforce'        => false,
        'nonexistant3'          => false,
      ],
      'unchanged' => [
        'nonexistant1'          => false,
        'nonexistant2'          => false,
        'config_enforce'        => false,
        'config_enforce_devel'  => false,
        'nonexistant3'          => false,
      ],
    ];

    /** @var array[] Arrays of expected implementations ordering after being altered. */
    $implementationsExpected = [
      'first' => [
        'nonexistant1'          => false,
        'nonexistant2'          => false,
        'config_enforce'        => false,
        'config_enforce_devel'  => false,
        'nonexistant3'          => false,
      ],
      'last' => [
        'nonexistant1'          => false,
        'nonexistant2'          => false,
        'config_enforce'        => false,
        'config_enforce_devel'  => false,
        'nonexistant3'          => false,
      ],
      'mixed' => [
        'nonexistant1'          => false,
        'nonexistant2'          => false,
        'config_enforce'        => false,
        'config_enforce_devel'  => false,
        'nonexistant3'          => false,
      ],
      'unchanged' => [
        'nonexistant1'          => false,
        'nonexistant2'          => false,
        'config_enforce'        => false,
        'config_enforce_devel'  => false,
        'nonexistant3'          => false,
      ],
    ];

    foreach (\array_keys($implementations) as $type) {

      $this->moduleHandler->alter(
        'module_implements', $implementations[$type], $hookName
      );

      // We need to test the order of the keys, not their values, so to do that
      // we have to pass the array keys themselves as arrays with numerical
      // indexes so that PHPUnit checks that each specific index is identical
      // to the expected one; without this, it wouldn't take the key order into
      // account and this would always pass as long as each key was present at
      // any location in both arrays; in other words, this makes PHPUnit check
      // that index 0 is the exact same in both arrays, index 1, index 2, and
      // so on, rather than that the 'nonexistant1' key exists in both arrays
      // regardless of order, 'nonexistant2' exists, 'nonexistant3' exists, and
      // so on.
      $this->assertEquals(
        \array_keys($implementationsExpected[$type]),
        \array_keys($implementations[$type])
      );

    }

  }

}
