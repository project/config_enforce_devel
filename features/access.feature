@settings @api
Feature: Access.
  In order to enforce configs
  As an Administrator
  I need to trust that non-admin users will not be able to access config_enforce_devel settings.

  Scenario: Devel Settings unavailable without relevant permissions
    Given I am logged in as a user with the "access content" permission
     When I go to "admin/config/development/config_enforce"
     Then I should not see "Devel settings"
