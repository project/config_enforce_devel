@add-target-module @api
Feature: Add a target module.
  In order to enforce configs
  As a developer
  I need to create target modules easily.

  Background:
    Given I am logged in as an "Administrator"

  @javascript @wip
  Scenario:
    Given I go to "/admin/config/development/config_enforce/settings"
     When I click "Add module"
      And I wait for AJAX to finish
     Then I should see "Create new target module"
     When I enter "Test Target Module" for "Name"
      And I enter "TEST_TARGET_MODULE" for "Machine name"
# This complains that the button isn't visible...
# Observing through VNC, we can manually click it to complete this test
#      And I press the "Create module" button
      And I wait for AJAX to finish
     Then I should see "Created Test Target Module (TEST_TARGET_MODULE) module at modules/custom/TEST_TARGET_MODULE."
#     Then the following files should exist:
#| files |
#| web/modules/custom/TEST_TARGET_MODULE |
#| web/modules/custom/TEST_TARGET_MODULE/TEST_TARGET_MODULE.info.yml |
#| web/modules/custom/TEST_TARGET_MODULE/config/install/config_enforce.registry.TEST_TARGET_MODULE.yml |

