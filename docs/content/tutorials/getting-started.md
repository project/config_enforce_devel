---
title: Getting Started
weight: 20

---

1. Enable `config_enforce_devel`, via the modules admin page, or via Drush: `drush pm-enable -y config_enforce_devel`
    1. This will enable `config_devel` (as a dependency), which will write any enforced configs to disk automatically, including Registries (see Terminology, above)
2. Visit the Enforced Configs page, available from the Toolbar menu or via the main menu Admin >> Configuration >> Development >> Config Enforce >> Enforced Configs (admin/config/development/config_enforce/enforced_configs).
    1. The first time that you visit this page, it will create a new Target Module for you called `config_enforce_default` (unless one already exists).
    2. This page contains:
        1. The names of all enforced configs, along with their enforcement settings. As a convenience, the names of config objects that were enforced from their config form will be rendered as links to their corresponding config forms.
        2. Buttons to "Generate from modules" or "Generate from active storage", which allow new config enforcement settings to be generated, either from previously exported config, or from the database (see Adding Config Enforce to an existing project, below)
        3. Controls for bulk operations that allow updates to, or deletion of, config enforcement settings for any selected configs.
3. Visit the "Settings" tab, or via the main menu *Admin >> Configuration >> Development >> Config Enforce >> Settings* (*admin/config/development/config_enforce/settings*)
    1. This page contains:
        1. An "Add module" button that will create a new target module for you.
        2. A list of modules available to set as target modules
        3. The default values to apply to new enforced configs
        4. A list of config objects to ignore from config enforcement.
    2. These settings are stored in a `config_enforce_devel.settings` config object, which you can enforce like any other config, so they will persist across reinstalls or developer environments. Typically you would store these in a "dev" target module which will not be enabled in production, since you won't have Config Enforce Devel there.


