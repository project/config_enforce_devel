---
title: Installation
weight: 10

---

Run the following commands:

```
composer require drupal/config_enforce
composer require --dev drupal/config_enforce_devel
composer config extra.enable-patching "true"
```

This should result in the following in your project's `composer.json`:

```
    "require": {
        "drupal/config_enforce": "^1.0",
[...]
    "require-dev": {
        "drupal/config_enforce_devel": "^1.0",
[...]
    "extra": {
        "enable-patching": true,
[...]
```

Then enable `config_enforce` by making it a dependency of your installation profile, a custom "site" module, on the modules admin page, or via Drush:

```
drush pm-enable -y config_enforce
```

