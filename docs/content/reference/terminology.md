---
title: Terminology
---

**Enforcement**: the goal of Config Enforce is to forcibly update active config from disk, to ensure that production config doesn't deviate from what's in code. This makes updating production config trivial, since we can update the code, refresh active config from there, and not worry about differences between them.

**Enforcement Level**: Config Enforce allows you to specify different levels of enforcement, depending on how rigidly you want to control the config. Currently there are 3 levels of enforcement:
  * **Allow form & API updates** will treat config objects as usual, installing them, but doesn't otherwise restrict changes in production or force updates from disk.
  * **Allow only API updates** will disable standard config forms from updating the config object, but allows programmatic updates to config, such as via `hook_update()` functions or the like.
  * **Read-only, no updates** further restricts *any* update to the config object entirely, including via API or programmatic updates. This enforcement level will also forcibly update active config with what's on disk when caches are rebuilt.

**Registry**: config enforce maintains a registry of enforced configs for each target module on a site, to track which config objects should be enforced, at what level, and in which config directory (`config/install` or `config/optional`). The registry also stores a hash of the config object itself, which is used to compare with active config and determine if a given config object needs updating.

**Target Module**: a module, theme, or install profile (any Drupal extension) that is configured within Config Enforce to contain enforced configs, and therefore has a registry and a set of `.yml` files it manages.


