---
title: Security Model
weight: 20

---

Config Enforce itself only applies exported configs under its control. It does not allow for the enforcement settings to be changed directly. This is an intentional element of its security model.

The Config Enforce Devel module provides a convenient UI to allow for managing enforced configs and their enforcement settings. It integrates with [Config Devel](https://drupal.org/project/config_devel) to automatically write configs to disk.

However, Config Enforce Devel is maintained as a seperate project, because it needs to be installed *only* in development environments. As such, you are *strongly discouraged* from deploying Config Enforce Devel outside of development environments.

