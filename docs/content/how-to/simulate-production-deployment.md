---
title: How to simulate deployment to a production environment
menuTitle: Simulate production deployment
weight: 40

---

To observe how the operational aspect of Config Enforce works, you can simulate a deployment with changes to the `system.site.yml`:

1. Ensure you have at least one config object enforced, eg. `system.site.yml`.
2. `drush pmu config_enforce_devel` to disable the developer UI, leaving only Config Enforce enabled.
3. Manually change the `system.site.yml` file, making a simple change to the *Site Name* or other string, and removing/altering the `hash` value so it differs from what's in active config
    1. Config Enforce Devel will automatically recalculate this hash each time it updates a config object, so when the new registry is deployed to production it will pick up the change
4. `drush cr` to rebuild caches, causing Config Enforce to re-scan all enforced configs and detect differing content hashes, then re-import any config objects which have changed.
5. Return to your site config form (eg. *Basic site settings*) and observe the simple string change has taken effect.
    1. Notice the config form itself is disabled (assuming you chose a high enough enforcement level), and you can't edit these settings directly anymore.
    2. The same principle applies to more complex config objects like Views, Page manager pages, Field settings, etc.


