---
title: How to generate enforcement settings from active storage
menuTitle: Generate settings from active storage
weight: 30

---

If there are configuration objects that are not yet in a target module on disk,
you can instead generate enforcement settings for them from Active config
storage.

In this case, when you click *Generate from active storage*, you will need to
select the Target module to save them into, as well as the Enforcement level
and config directory. The multi-select box below will show you all
configuration objects in Active storage that are not explicitly ignored by your
Config Enforce Devel settings (see below).

Once again, after selecting all the configuration objects you want to enforce,
and setting the Target module, Directory, and Enforcement level, and clicking
Save, these configuration objects will be written to disk in the Target module
you selected, and an entry is created for them in the Registry.

