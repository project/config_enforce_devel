---
title: How to apply enforced configs
menuTitle: Applying enforced configs
weight: 35

---

Once you've added some enforced configs (see Next Steps, below), you can import them by simply rebuilding caches:

```
drush cr
```

You can then check the log to see if any enforced configs have been imported.
